import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  name= "Angular is a type-script framwork";
  user=
  {
    name: "Angular",
    version: "16x",
    type: "UI framewrok"
  }

  fruitsList=['mango', 'banana', 'watermellon', 'apple', 'grapes'];

  receiveUserData(data:any)
  {
    console.log(data);
    alert(data)
  }

  subjectName="angular";
  onChange()
  {
    alert(this.subjectName)
  }

  arr=['ramu','rabi','saddam','manas','arnab'];

  isValid=true;

  fruit='mango';

  mobileName="nokia";
  
}
 

  


