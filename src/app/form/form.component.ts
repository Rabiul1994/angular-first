import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onChange(formData:any)
  {
    console.log(formData)
    console.log(formData.value)
  }

  formSubmit(form:any)
  {
    console.log(form)
  }
}
