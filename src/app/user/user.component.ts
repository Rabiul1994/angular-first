import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ColDef } from 'ag-grid-community';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(
    private commonService:CommonService,
    private route:Router
  ) { }

  ngOnInit(): void {
  }

  userList:any;
  public rowData:any;
  getUsers()
  {
    this.commonService.getUserData().subscribe(resp=>{
      let temp:any=resp;
      this.userList=temp.data;
      this.rowData=this.userList;
     
    });

  }

  dataList:any;
  @Output() event=new EventEmitter();
  viewData(data:any)
  {
    
    this.event.emit(data);
  }

  getData()
  {
      this.commonService.getData().subscribe(res=>{
      this.dataList=res;
      
    });

  }

  public columnDefs: ColDef[] = [
    { field: 'first_name'},
    { field: 'last_name'},
    { field: 'email'},
    { field: 'avatar'}
   
  ];

  public defaultColDef: ColDef = {
    sortable: true,
    filter: true,
  };

  

}
