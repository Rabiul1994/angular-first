import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-csk',
  templateUrl: './csk.component.html',
  styleUrls: ['./csk.component.css']
})
export class CskComponent implements OnInit {

  constructor(
    public commonService:CommonService
  ) { }

  ngOnInit(): void {
  }

  msg=this.commonService.getMsg();

}
