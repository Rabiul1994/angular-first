import { FormComponent } from './form/form.component';
import { ReactiveFormComponent } from './reactive-form/reactive-form.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {
    path:'reactiveForm',
   component: ReactiveFormComponent
  //  loadChildren: ()=>import("./app.module").then(m=>m.AppModule)
  },
  {
    path:'user',
    component: UserComponent
  },
  {
    path:'form',
    component: FormComponent
  },
  {
    path:'first',
    loadChildren:()=>import('./first/first.module').then(m=>m.FirstModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
