import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-rcb',
  templateUrl: './rcb.component.html',
  styleUrls: ['./rcb.component.css']
})
export class RcbComponent implements OnInit {

  constructor(
    public commonService:CommonService
  ) { }

  ngOnInit(): void {
  }

  msg=this.commonService.getMsg();

}
