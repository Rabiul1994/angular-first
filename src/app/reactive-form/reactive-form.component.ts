import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

  public color:any;

  public reactiveForm= new FormGroup(
    {
      name: new FormControl('', [Validators.required, Validators.minLength(5)]),
      email: new FormControl('', Validators.required),
      contact: new FormControl('', Validators.required)
    }
  )

  formSubmit()
  {
    console.log(this.reactiveForm)
  }

  getFormValue()
  {
    console.log(this.reactiveForm)
  }

  navigate()
  {
    console.log("executing navigation");
    this.router.navigate(['/user'])
  }

  onClick()
  {
    this.color={'background-color':'green'}
    console.log(this.color);
  }

}
