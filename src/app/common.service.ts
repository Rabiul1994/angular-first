import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private httpClient:HttpClient
  ) { }

  message="Indian Premier League team";

  getMsg()
  {
    return this.message;
  }

  getUserData()
  {
    // https://reqres.in/api/users?page=2  -> url taken from from fake api

   return this.httpClient.get("https://reqres.in/api/users?page=2");
   
  }

  getData()
  {
    let url=`https://jsonplaceholder.typicode.com/posts`
    return this.httpClient.get(url);
  }
}
